<?php

require_once __DIR__ . '/vendor/autoload.php';

$connection = new \Nette\Database\Connection('mysql:host=sql16.web4u.cz;dbname=pocitace', 'schor', 'HesloJeTajne', [
	'lazy' => true,
]);
$structure = new \Nette\Database\Structure($connection, new \Nette\Caching\Storages\DevNullStorage());
$database = new \Nette\Database\Context($connection, $structure);
$questionRepo = new \App\Question\QuestionRepository($database);
$answerRepo = new \App\Answer\AnswerRepository($database);
$questionFacade = new \App\Question\QuestionFacade($questionRepo, $answerRepo);
$controller = new \App\Controller\MainController($questionFacade);

//run app
if (isset($_GET['data']) && $_SERVER['REQUEST_METHOD'] === 'GET') {
	$controller->actionQuestions();
} else {
	$controller->actionDefault();
}