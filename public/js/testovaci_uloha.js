let questions;

function saveResult(p) {
  if (isLocalStorageAvailable()) {
    let results;

    if (localStorage.getItem("results") === null) {
      results = [];
    } else {
      results = JSON.parse(localStorage.getItem("results"));
    }

    let now = new Date();
    let date = now.toLocaleString();
    results.push({
      date: date,
      percent: p
    });
    localStorage.setItem("results", JSON.stringify(results));
    printResult(date,p);
  }
}

function printResults() {
  if (isLocalStorageAvailable()) {
    if (localStorage.getItem("results") !== null) {
      let results = JSON.parse(localStorage.getItem("results"));
      $.each(results, function(key, value) {
        printResult(value.date, value.percent);
      });
    }
  }
}

function printResult(d,p) {
  $("#quiz-results").append("<div>" + d + ": " + p + " %</div>");
}

function deleteResults () {
  if (isLocalStorageAvailable()) {
    localStorage.clear();
    $("#quiz-results").empty();
  }
}

function isLocalStorageAvailable() {
  return typeof(Storage) !== "undefined";
}

(function() {
  "use strict";

  let questionIndex,
    currentQuestion,
    score,
    timeSpent,
    questionIsAnswered,
    isQuizDone;
  let quiz = document.getElementById("quiz");

  function initQuiz() {
    quiz.classList.remove("quiz-intro");
    quiz.classList.add("quiz-started");

    questionIndex = 0;
    currentQuestion = 1;
    questionIsAnswered = 0;
    score = 0;
    timeSpent = "00:00";

    quiz.innerHTML = `<div id="progress-container"><span id="progress"></span></div>
    <div id="stats">
    <p>Otázka: <span id="questionNumber">${currentQuestion}/${
      questions.length
    }</span></p>
    <p>Skóre: <span id="score">${score}</span></p>
    </div>
    <section id="answers"></section>`;

    displayQuestion();
  }

  function displayQuestion() {
    let question = questions[questionIndex];
    let answers = document.getElementById("answers");
    let answerNumber = 0;
    let output = `<h2 class="text-center bold">${currentQuestion}. ${
      question.question
    }</h2>`;

    for (let i in question.answers) {
      answerNumber++;
      output += `<div class="answer">
      <input type="radio" id="answer-${answerNumber}" name="answers" value="${answerNumber}">
      <label for="answer-${answerNumber}">
      <span class="answer-number">${answerNumber}.</span> ${question.answers[i]}
      </label>
      </div>`;
    }

    answers.innerHTML = output;
  }

  function displayResults() {
    let notification = document.getElementById("notification");
    notification.parentElement.removeChild(notification);
    isQuizDone = 1;

    let pageURL = window.location.href;
    let shareText = `I just finished this quiz and got ${score} out of ${
      questions.length
    } questions right.`;
    let fbShareURL = `https://www.facebook.com/sharer.php?u=${pageURL}&quote=${shareText}`;
    let twitterShareURL = `https://twitter.com/intent/tweet?text=${shareText} ${pageURL}`;

    quiz.innerHTML = `<section id="results" class="text-center">
    <h2 class="bold">Vaše výsledky:</h2>
    <p id="percentage">${scorePercentage()}%</p>
    <p>Zodpovězené otázky <span class="bold">${score}</span> ze <span class="bold">${
      questions.length
    }</span> otázek.</p>

    <button type="button" id="start-over-btn" class="btn blue-btn">Spustit znovu</button>
    </section>`;
  }

  function goToNextQuestion() {
    currentQuestion++;
    questionIndex++;
    questionIsAnswered = 0;

    let notification = document.getElementById("notification");
    notification.parentElement.removeChild(notification);

    let questionNumber = document.getElementById("questionNumber");
    questionNumber.textContent = `${currentQuestion}/${questions.length}`;

    displayQuestion();
  }

  function submitAnswer(e) {
    let selectedAnswer = Number(e.target.value);
    let rightAnswer = questions[questionIndex].correctAnswer;
    let answers = document.getElementsByName("answers");
    let progress = document.getElementById("progress");

    questionIsAnswered = 1;

    progress.style.width = progressPercentage() + "%";

    let notification = document.createElement("div");
    let message = document.createElement("p");
    let label = e.target.nextElementSibling;
    notification.id = "notification";

    if (selectedAnswer === rightAnswer) {
      score++;
      message.textContent = "Správná odpověď!";
      label.classList.add("green-bg");
    } else {
      message.textContent = "Špatná odpověď!";
      label.classList.add("red-bg");

      answers.forEach(answer => {
        if (Number(answer.value) !== rightAnswer) return;

        answer.nextElementSibling.classList.add("green-bg");
      });
    }

    let button = document.createElement("button");
    button.classList.add("blue-btn");

    if (isLastQuestion()) {
      button.id = "show-results-btn";
      button.textContent = "Zobrazit výsledky";
    } else {
      button.id = "next-btn";
      button.textContent = "Pokračovat";
    }

    notification.appendChild(message);
    notification.appendChild(button);
    quiz.insertAdjacentElement("afterend", notification);

    button.focus();

    answers.forEach(answer => (answer.disabled = "disabled"));

    document.getElementById("score").textContent = score;
  }

  let scorePercentage = () => (score / questions.length * 100).toFixed(0);
  let progressPercentage = () =>
    (currentQuestion / questions.length * 100).toFixed(0);
  let isLastQuestion = () => currentQuestion === questions.length;

  function spaceBarHandler() {
    if (document.querySelector(".quiz-intro")) {
      initQuiz();
    }

    if (questionIsAnswered) {
      goToNextQuestion();
    }

    if (!isQuizDone) {
      displayResults();
      console.log("last");
    }
  }

  function numericKeyHandler(e) {
    let answers = document.getElementsByName("answers");

    answers.forEach(answer => {
      if (answer.value === e.key) {
        if (questionIsAnswered) return;

        answer.checked = "checked";

        let event = new Event("change");
        answer.dispatchEvent(event);
        submitAnswer(event);

        questionIsAnswered = 1;
      }
    });
  }

  document.addEventListener("click", function(e) {
    if (
      e.target.matches("#start-quiz-btn") ||
      e.target.matches("#start-over-btn")
    ) {
      $.getJSON("?data=true", function(result){
        questions = [];
        $.each(result, function(i, question){
          questions.push(question);
        });
        initQuiz();
      });
    }
    if (e.target.matches("#next-btn")) goToNextQuestion();
    if (e.target.matches("#show-results-btn")) {
      displayResults();
      saveResult(scorePercentage());
    }
  });

  document.addEventListener("change", function(e) {
    if (e.target.matches('input[type="radio"]')) submitAnswer(e);
  });

  document.addEventListener("keyup", function(e) {
    if (e.keyCode === 32) spaceBarHandler(); // init quiz / go to next question
    if (e.keyCode >= 48 && e.keyCode <= 57) numericKeyHandler(e); // choose an answer
  });

  document
    .getElementById("shortcuts-info-btn")
    .addEventListener("click", function() {
      let info = document.querySelector(".shortcuts-info");
      info.classList.toggle("display-block");
    });
})();
