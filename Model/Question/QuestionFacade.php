<?php

namespace App\Question;

use App\Answer\AnswerRepository;

final class QuestionFacade
{
	/**
	 * @var QuestionRepository
	 */
	private $questionRepo;

	/**
	 * @var AnswerRepository
	 */
	private $answerRepo;

	/**
	 * QuestionFacade constructor.
	 *
	 * @param QuestionRepository $questionRepo
	 * @param AnswerRepository $answerRepo
	 */
	public function __construct(QuestionRepository $questionRepo, AnswerRepository $answerRepo)
	{
		$this->questionRepo = $questionRepo;
		$this->answerRepo = $answerRepo;
	}

	/**
	 * @param int $limit
	 * @return array
	 */
	public function getQuestions($limit = 3)
	{
		$data = [];
		$questions = $this->questionRepo->findRandoms();
		$answers = $questions ? $this->answerRepo->findByMoreQuestionId(array_map(function (Question $question) {
			return $question->getId();
		}, $questions)) : [];

		foreach ($questions as $question) {
			$collection = [];
			$collection['question'] = $question->getQuestion();

			foreach (isset($answers[$question->getId()]) ? $answers[$question->getId()] : [] as $key => $questionAnswer) {
				$collection['answers'][] = $questionAnswer->getAnswer();
				if ($questionAnswer->isRight()) {
					$collection['correctAnswer'] = $key + 1;
				}
			}

			$data[] = $collection;
		}

		return $data;
	}
}