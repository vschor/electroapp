<?php

namespace App\Question;

use Nette\Database\Context;
use Nette\Database\IRow;

final class QuestionRepository
{
	/**
	 * @var Context
	 */
	private $database;

	/**
	 * QuestionRepository constructor.
	 *
	 * @param Context $database
	 */
	public function __construct(Context $database)
	{
		$this->database = $database;
	}

	/**
	 * @param int $limit
	 * @return Question[]|array
	 */
	public function findRandoms($limit = 3)
	{
		$questions = $this->database->table('otazka')
			->order('RAND()')
			->limit($limit)
			->fetchAll();

		return $this->createQuestions($questions);
	}

	/**
	 * @param IRow[]
	 * @return Question[]
	 */
	private function createQuestions(array $rows)
	{
		$questions = [];
		foreach ($rows as $row) {
			$questions[] = new Question($row['id'], $row['otazka']);
		}
		return $questions;
	}
}