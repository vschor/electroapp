<?php

namespace App\Question;

final class Question
{
	/**
	 * @var int
	 */
	private $id;

	/**
	 * @var string
	 */
	private $question;

	/**
	 * Question constructor.
	 *
	 * @param int $id
	 * @param string $question
	 */
	public function __construct($id, $question)
	{
		$this->id = $id;
		$this->question = $question;
	}

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @return string
	 */
	public function getQuestion()
	{
		return $this->question;
	}
}