<?php

namespace App\Answer;

use Nette\Database\Context;
use Nette\Database\IRow;

final class AnswerRepository
{
	/**
	 * @var Context
	 */
	private $database;

	/**
	 * AnswerRepository constructor.
	 *
	 * @param Context $database
	 */
	public function __construct(Context $database)
	{
		$this->database = $database;
	}

	/**
	 * @param array $questionId
	 * @return Answer[][]
	 */
	public function findByMoreQuestionId(array $questionId)
	{
		$answers = $this->database->table('odpoved')
			->where('otazka_id IN ? ', $questionId)
			->order('RAND()')
			->fetchAll();

		return $this->createAnswers($answers);
	}

	/**
	 * @param IRow[]
	 * @return Answer[][]
	 */
	private function createAnswers(array $rows)
	{
		$answers = [];
		foreach ($rows as $row) {
			$answers[$row['otazka_id']][] = new Answer(
				$row['id'],
				$row['otazka_id'],
				$row['odpoved'],
				(bool)$row['spravna']);
		}
		return $answers;
	}
}