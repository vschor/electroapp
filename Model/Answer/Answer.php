<?php

namespace App\Answer;

final class Answer
{
	/**
	 * @var int
	 */
	private $id;

	/**
	 * @var int
	 */
	private $questionId;

	/**
	 * @var string
	 */
	private $answer;

	/**
	 * @var bool
	 */
	private $isRight;

	/**
	 * Answer constructor.
	 *
	 * @param int $id
	 * @param int $questionId
	 * @param string $answer
	 * @param bool $isRight
	 */
	public function __construct($id, $questionId, $answer, $isRight)
	{
		$this->id = $id;
		$this->questionId = $questionId;
		$this->answer = $answer;
		$this->isRight = $isRight;
	}

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @return int
	 */
	public function getQuestionId()
	{
		return $this->questionId;
	}

	/**
	 * @return string
	 */
	public function getAnswer()
	{
		return $this->answer;
	}

	/**
	 * @return bool
	 */
	public function isRight()
	{
		return $this->isRight;
	}
}