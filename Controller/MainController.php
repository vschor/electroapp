<?php

namespace App\Controller;

use App\Question\QuestionFacade;

final class MainController
{
	/**
	 * @var QuestionFacade
	 */
	private $questionFacade;

	/**
	 * MainController constructor.
	 *
	 * @param QuestionFacade $questionFacade
	 */
	public function __construct(QuestionFacade $questionFacade)
	{
		$this->questionFacade = $questionFacade;
	}

	/**
	 * @return void
	 */
	public function actionDefault()
	{
		ob_start();
		require_once __DIR__ . '/../View/template_index.php';
		ob_flush();
	}

	/**
	 * @return void
	 */
	public function actionQuestions()
	{
		$data = $this->questionFacade->getQuestions();

		header('Content-Type: application/json');
		echo json_encode($data);
		exit();
	}
}