<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
	<meta name="generator" content="PSPad editor, www.pspad.com">
	<link rel="stylesheet" href="public/css/elektro.css">
	<link rel="stylesheet" href="public/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<title>Elektro</title>
</head>
<body>
<div class="banner"></div>
<div class="obsah">
	<div class="row justify-content-center">
		<div class="col-6">
			<main role="main">
				<article id="quiz" class="quiz-intro">
					<div id="quiz-bg"><iframe src="https://youtube.com/embed/ku-JnJ43bQA" frameborder="10" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
					<section id="quiz-body">
						<h1>Elektrotechnika - Laboratorní úloha</h1>
						<p>Laboratorní úloha: měření voltampérové charakteristiky žárovky<br>Přehrajte si video a potom odpovězte na otázky</p>
						<button type="button" id="start-quiz-btn" class="btn blue-btn rounded-corners">SPUSTIT TEST</button>
					</section>
				</article>
			</main>
            <hr>
            <div class="row">
				<div class="col-6">
					<h5>Poslední výsledky testů</h5>
				</div>
				<div class="col-6 text-right">
					<button onclick="deleteResults()" class="btn blue-btn btn-sm">smazat výsledky</button>
				</div>
                <div class="col-12">
                    <div id="quiz-results"></div>
                </div>
			</div>
		</div>
	</div>
</div>
<footer>
	<div class="row justify-content-center">
		<div class="container">Elektro - Vlastimil Schoř - <a href="/elektro">Domů</a></div>
	</div>
</footer>
<a href="javascript:" id="return-to-top"><span>^</span></a>
</body>
<script src="public/js/bootstrap.js"></script>
<script src="public/js/jquery-3.3.1.min.js"></script>
<script src="public/js/testovaci_uloha.js"></script>
<script type="text/javascript">
    $(window).scroll(function() {
        if ($(this).scrollTop() >= 50) {
            $('#return-to-top').fadeIn(300);
        } else {
            $('#return-to-top').fadeOut(300);
        }
    });
    $('#return-to-top').click(function() {
        $('body,html').animate({
            scrollTop : 0
        }, 500);
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        printResults();
    });
</script>
</html>
